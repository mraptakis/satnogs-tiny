#!/usr/bin/env python3

import argparse
from tuning_mode_pb2 import *
import paho.mqtt.client as mqtt

BROKER_HOSTNAME = 'localhost'
BROKER_PORT = 1883
BROKER_TIMEOUT = 60


def mqtt_connect(hostname: str = BROKER_HOSTNAME,
				port: int = BROKER_PORT,
				timeout: int = BROKER_TIMEOUT,
				username: str = '',
				password: str = '') -> mqtt.Client:
	'''
	Connects to mqtt broker and returns the client
	'''
	client = mqtt.Client()
	client.username_pw_set(username, password)
	try:
		client.connect(hostname,
					   port,
					   timeout)
	except ConnectionRefusedError:
		print("ERROR: Connection refused, maybe no MQTT broker "
			  "running at {}:{} ?".format(hostname, port))

	return client

def set_example_tuning_mode(station_id: int):
	'''
	Set the tuning mode (modulation and framing paramenters)
	in the selected station to example values.
	'''
	client = mqtt_connect()

	fsk_params = fsk()
	fsk_params.baudrate = 9600.0
	fsk_params.modulation_index = 1.0
	fsk_params.preamble = b"\xF0\xF0\xF0\xF0"
	fsk_params.sync_word = b"\x91\x12\x99"
	fsk_params.frame_size_fixed = True
	fsk_params.frame_size = 41
	fsk_params.crc = fsk.NO_CRC
	fsk_params.whitening = fsk.NO_WHITENING
	
	tmode = tuning_mode()
	tmode.mode = tuning_mode.FSK
	tmode.fsk_params.CopyFrom(fsk_params)
	payload = tmode.SerializeToString()

	client.publish('station/{}/mode'.format(station_id),
				   payload=payload,
				   qos=0,
				   retain=False)
	print("Sent to {}: \n{}".format(station_id, tmode))


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("--station",
						help="The station ID",
						type=int,
						required=True)
	args = parser.parse_args()

	set_example_tuning_mode(station_id=args.station)
