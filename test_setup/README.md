# SatNOGS Tiny Unit test setup

## Updating the serilization schema

When the protocol buffer scheme was updated, the compiled de-/serializers have
to be re-generated. For the python interface this can be done with
the following command:
```
cd test_setup
protoc -I=../proto --python_out=./ ../proto/satnogs-tiny.proto
```

This will create the file `satnogs_tiny_pb2.py` in the `test_setup` folder.

## Testing satnogs-tiny locally
### Installation / Setup

The following packages are needed:
- mosquitto
- docker
- python

Create python virtual environment and install required python packages:
```
cd test_setup
mkvirtualenv satnogs-tiny
pip install -r requirements.txt
```

Start the services (currently the MQTT broker only):
```
cd test_setup
docker-compose up -d
```

### Usage

The MQTT broker is running on `localhost:1883`, so the satnogs-tiny station can connect to it.
Once connected you can set the modulation parameters on station 42 to some default value with:
```
./scripts/set_modulation.py --station 42
```
