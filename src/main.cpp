#include <Arduino.h>
#include <station.hpp>

station &s = station::instance();

void setup() {
  Serial.begin(115200);
  s.setup();
}

void loop() {
  s.loop();
}
