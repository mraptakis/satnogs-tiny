#include "status.hpp"
#include <ESPNtpClient.h>

status::status()
{
}

void
status::set_from_proto(const ground_station_status<20> &params)
{
  strcpy(d_current_datetime, params.get_current_datetime().get_const());
  d_rx = params.get_in_rx().get();
  d_mode = static_cast<modulation_mode>(params.get_mode());

  if (d_rx) {
    d_rx_freq = params.get_rx_freq().get();
  }
  d_frames_cnt = params.get_frames_cnt().get();
}

void
status::update_datetime()
{
  String datetime = NTP.getTimeDateStringUs();
  strncpy(d_current_datetime, datetime.c_str(), 19);
}

bool
status::in_rx() const
{
  return d_rx;
}
