#include "mqtt_client.hpp"

mqtt_client::mqtt_client()
{
}

void
mqtt_client::msg_received(MQTTClient *client, uint8_t *topic, uint8_t *bytes, size_t length)
{
  Serial.printf("topic: %s, length of payload: %d\n", topic, length);

  d_msg_received = true;
  d_rb.clear();
  for (size_t i = 0; i < length; ++i) {
    d_rb.push(bytes[i]);
  }

  String station_topic_prefix = String(MQTT_TOPIC_PREFIX) + String(d_cnf.get_station_id());
  if (!strncmp((station_topic_prefix + "/mode").c_str(), (char*) topic, strlen((char*) topic))) {
    d_topic = mqtt_client::topic::MODE;
  }
  else if (!strncmp((station_topic_prefix + "/frequency").c_str(), (char*) topic, strlen((char*) topic))) {
    d_topic = mqtt_client::topic::FREQUENCY;
  }
  else if (!strncmp((station_topic_prefix + "/tx/frame").c_str(), (char*) topic, strlen((char*) topic))) {
    d_topic = mqtt_client::topic::TX_FRAME;
  }
  else {
    Serial.println("ERROR: invalid topic for receive");
  }
}

bool
mqtt_client::connect_credentials()
{
  bool result;
  String client_id = String(d_cnf.get_station_id()) + String("-") + String(d_cnf.getThingName());
  if (d_cnf.get_mqtt_pass().c_str()[0] != '\0') {
    result = d_client.connect(client_id.c_str(), d_cnf.get_mqtt_username().c_str(), d_cnf.get_mqtt_pass().c_str());
  }
  else if (d_cnf.get_mqtt_username().c_str()[0] != '\0') {
    result = d_client.connect(client_id.c_str(), d_cnf.get_mqtt_username().c_str());
  }
  else {
    result = d_client.connect(client_id.c_str());
  }

  return result;
}

void
mqtt_client::subscribe()
{
  /*
   * station/id/
   *  +
   *  |--> status     // used by the client to publish operational status, etc
   *  |--> mode       // broker publishes the mode to tune the SX1278 IC (LoRa, FSK, rates etc)
   *  |--> frequency  // broker publishes the Doppler information
   *  |--> frame      // client publishes decoded (fully or partially) data
   *  | tx
   *    +
   *    |--> frame    // Broker publishes the frame to be transmitted
   */
  String station_topic_prefix = String(MQTT_TOPIC_PREFIX) + String(d_cnf.get_station_id());
  Serial.printf("Subscribe to '%s' topic... ", (station_topic_prefix + String("/status")).c_str());
  d_client.subscribe(station_topic_prefix + String("/status"));
  Serial.println("Success");
  Serial.printf("Subscribe to '%s' topic... ", (station_topic_prefix + String("/mode")).c_str());
  d_client.subscribe(station_topic_prefix + String("/mode"));
  Serial.println("Success");
  Serial.printf("Subscribe to '%s' topic... ", (station_topic_prefix + String("/frequency")).c_str());
  d_client.subscribe(station_topic_prefix + String("/frequency"));
  Serial.println("Success");
  Serial.printf("Subscribe to '%s' topic... ", (station_topic_prefix + String("/frame")).c_str());
  d_client.subscribe(station_topic_prefix + String("/frame"));
  Serial.println("Success");
  Serial.printf("Subscribe to '%s' topic... ", (station_topic_prefix + String("/tx/frame")).c_str());
  d_client.subscribe(station_topic_prefix + String("/tx/frame"));
  Serial.println("Success");
}

void
mqtt_client::init(bool valid_config)
{
  if (!valid_config) {
    d_cnf.mqtt_clear_credentials();
  }
  d_client.begin(d_cnf.get_mqtt_server().c_str(), d_cnf.get_mqtt_port(), d_wifi_client);
  d_client.onMessageAdvanced([this] (MQTTClient *client, char topic[], char bytes[], int length) {
    msg_received(client, (uint8_t*) topic, (uint8_t*) bytes, (size_t) length);
  });
}

bool
mqtt_client::connect()
{
  Serial.print("Connecting to MQTT server (if this takes longer than expected, then check the credentials)... ");
  while (!connect_credentials()) {
    delay(1000);
  }
  Serial.println("Success");

  subscribe();  

  return true;
}

bool
mqtt_client::connected()
{
  return d_client.connected();
}

void
mqtt_client::loop()
{
  d_client.loop();
}

void
mqtt_client::publish(topic t, wbuffer<BUFFER_SIZE> &wb)
{
  uint8_t payload[BUFFER_SIZE];
  for (size_t i = 0; i < wb.get_size(); ++i) {
    payload[i] = wb[i];
  }
  String station_topic_prefix = String(MQTT_TOPIC_PREFIX) + String(d_cnf.get_station_id());
  String sent_topic;
  switch (t) {
  case topic::STATUS:
    sent_topic = station_topic_prefix + "/status";
    break;
  case topic::FRAME:
    sent_topic = station_topic_prefix + "/frame";
    break;
  default:
    Serial.println("ERROR: invalid topic for publish");
    return;
  }
  d_client.publish(sent_topic.c_str(), (char*) payload, wb.get_size(), false, 0);  
}

bool
mqtt_client::has_receive_message() const
{
  return d_msg_received;
}

void
mqtt_client::message_was_read()
{
  d_msg_received = false;
}

const rbuffer<BUFFER_SIZE>&
mqtt_client::get_readbuffer() const
{
  return d_rb;
}

mqtt_client::topic
mqtt_client::get_topic() const
{
  return d_topic;
}
