#ifndef FSK_HPP
#define FSK_HPP

#include <stdint.h>
#include <stddef.h>
#include <protobuf.hpp>

class fsk_modulation {
public:
  enum class crc_scheme {
    NO_CRC = 0,
    CRC32_C,
    CRC16_CCIT
  };

  enum class whitening_scheme {
    NO_WHITENING = 0,
    G3RUH,
    CCSDS
  };

  fsk_modulation();

  fsk_modulation(const fsk_modulation &other);

  template<uint32_t preamble_LEN, uint32_t sync_word_LEN>
  fsk_modulation(const fsk<preamble_LEN, sync_word_LEN> &params)
  {
    d_baudrate = params.get_baudrate().get();
    d_modulation_index = params.get_modulation_index().get();
    d_preamble_len = params.get_preamble().get_length();
    d_preamble = new uint8_t[d_preamble_len];
    memcpy(d_preamble, params.get_preamble().get_const(), d_preamble_len);
    
    d_sync_word_len = params.get_sync_word().get_length();
    d_sync_word = new uint8_t[d_sync_word_len];
    memcpy(d_sync_word, params.get_sync_word().get_const(), d_sync_word_len);
    
    d_frame_size_fixed = params.get_frame_size_fixed().get();
    d_frame_size = params.get_frame_size().get();
    d_crc = static_cast<crc_scheme>(params.get_crc());
    d_whitening = static_cast<whitening_scheme>(params.get_whitening());
  }

  template<uint32_t preamble_LEN, uint32_t sync_word_LEN>
  void
  set_from_proto(const fsk<preamble_LEN, sync_word_LEN> &params)
  {
    d_baudrate = params.get_baudrate().get();
    d_modulation_index = params.get_modulation_index().get();
    d_preamble_len = params.get_preamble().get_length();
    d_preamble = new uint8_t[d_preamble_len];
    memcpy(d_preamble, params.get_preamble().get_const(), d_preamble_len);
    
    d_sync_word_len = params.get_sync_word().get_length();
    d_sync_word = new uint8_t[d_sync_word_len];
    memcpy(d_sync_word, params.get_sync_word().get_const(), d_sync_word_len);
    
    d_frame_size_fixed = params.get_frame_size_fixed().get();
    d_frame_size = params.get_frame_size().get();
    d_frame_size_fixed = params.get_frame_size_fixed().get();
    d_frame_size = params.get_frame_size().get();
    d_crc = static_cast<crc_scheme>(params.get_crc());
    d_whitening = static_cast<whitening_scheme>(params.get_whitening());
  }

  double
  get_baudrate() const;

  double
  get_modulation_index() const;

  const uint8_t*
  get_preamble() const;

  size_t
  get_preamble_length() const;

  const uint8_t*
  get_sync_word() const;

  size_t
  get_sync_word_length() const;

  bool
  has_fixed_frame_size() const;

  size_t
  get_frame_size() const;

  crc_scheme
  get_crc() const;

  whitening_scheme
  get_whitening() const;
  
private:
  double d_baudrate;
  double d_modulation_index;
  uint8_t *d_preamble = nullptr;
  size_t d_preamble_len;
  uint8_t *d_sync_word = nullptr;
  size_t d_sync_word_len;
  bool d_frame_size_fixed;
  size_t d_frame_size;
  crc_scheme d_crc;
  whitening_scheme d_whitening;
};

#endif // FSK_HPP
