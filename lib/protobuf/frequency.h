/*
 *  Copyright (C) 2020-2021 Embedded AMS B.V. - All Rights Reserved
 *
 *  This file is part of Embedded Proto.
 *
 *  Embedded Proto is open source software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 *  Embedded Proto  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Embedded Proto. If not, see <https://www.gnu.org/licenses/>.
 *
 *  For commercial and closed source application please visit:
 *  <https://EmbeddedProto.com/license/>.
 *
 *  Embedded AMS B.V.
 *  Info:
 *    info at EmbeddedProto dot com
 *
 *  Postal address:
 *    Johan Huizingalaan 763a
 *    1066 VH, Amsterdam
 *    the Netherlands
 */

// This file is generated. Please do not edit!
#ifndef FREQUENCY_H
#define FREQUENCY_H

#include <cstdint>
#include <MessageInterface.h>
#include <WireFormatter.h>
#include <Fields.h>
#include <MessageSizeCalculator.h>
#include <ReadBufferSection.h>
#include <RepeatedFieldFixedSize.h>
#include <FieldStringBytes.h>
#include <Errors.h>
#include <limits>

// Include external proto definitions


class doppler_offset final: public ::EmbeddedProto::MessageInterface
{
  public:
    doppler_offset() = default;
    doppler_offset(const doppler_offset& rhs )
    {
      set_millis(rhs.get_millis());
      set_freq(rhs.get_freq());
    }

    doppler_offset(const doppler_offset&& rhs ) noexcept
    {
      set_millis(rhs.get_millis());
      set_freq(rhs.get_freq());
    }

    ~doppler_offset() override = default;

    enum class id : uint32_t
    {
      NOT_SET = 0,
      MILLIS = 1,
      FREQ = 2
    };

    doppler_offset& operator=(const doppler_offset& rhs)
    {
      set_millis(rhs.get_millis());
      set_freq(rhs.get_freq());
      return *this;
    }

    doppler_offset& operator=(const doppler_offset&& rhs) noexcept
    {
      set_millis(rhs.get_millis());
      set_freq(rhs.get_freq());
      return *this;
    }

    inline void clear_millis() { millis_.clear(); }
    inline void set_millis(const EmbeddedProto::uint64& value) { millis_ = value; }
    inline void set_millis(const EmbeddedProto::uint64&& value) { millis_ = value; }
    inline EmbeddedProto::uint64& mutable_millis() { return millis_; }
    inline const EmbeddedProto::uint64& get_millis() const { return millis_; }
    inline EmbeddedProto::uint64::FIELD_TYPE millis() const { return millis_.get(); }

    inline void clear_freq() { freq_.clear(); }
    inline void set_freq(const EmbeddedProto::floatfixed& value) { freq_ = value; }
    inline void set_freq(const EmbeddedProto::floatfixed&& value) { freq_ = value; }
    inline EmbeddedProto::floatfixed& mutable_freq() { return freq_; }
    inline const EmbeddedProto::floatfixed& get_freq() const { return freq_; }
    inline EmbeddedProto::floatfixed::FIELD_TYPE freq() const { return freq_.get(); }


    ::EmbeddedProto::Error serialize(::EmbeddedProto::WriteBufferInterface& buffer) const override
    {
      ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;

      if((0U != millis_.get()) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        return_value = millis_.serialize_with_id(static_cast<uint32_t>(id::MILLIS), buffer, false);
      }

      if((0.0 != freq_.get()) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        return_value = freq_.serialize_with_id(static_cast<uint32_t>(id::FREQ), buffer, false);
      }

      return return_value;
    };

    ::EmbeddedProto::Error deserialize(::EmbeddedProto::ReadBufferInterface& buffer) override
    {
      ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;
      ::EmbeddedProto::WireFormatter::WireType wire_type = ::EmbeddedProto::WireFormatter::WireType::VARINT;
      uint32_t id_number = 0;
      id id_tag = id::NOT_SET;

      ::EmbeddedProto::Error tag_value = ::EmbeddedProto::WireFormatter::DeserializeTag(buffer, wire_type, id_number);
      while((::EmbeddedProto::Error::NO_ERRORS == return_value) && (::EmbeddedProto::Error::NO_ERRORS == tag_value))
      {
        id_tag = static_cast<id>(id_number);
        switch(id_tag)
        {
          case id::MILLIS:
            return_value = millis_.deserialize_check_type(buffer, wire_type);
            break;

          case id::FREQ:
            return_value = freq_.deserialize_check_type(buffer, wire_type);
            break;

          case id::NOT_SET:
            return_value = ::EmbeddedProto::Error::INVALID_FIELD_ID;
            break;

          default:
            return_value = skip_unknown_field(buffer, wire_type);
            break;
        }

        if(::EmbeddedProto::Error::NO_ERRORS == return_value)
        {
          // Read the next tag.
          tag_value = ::EmbeddedProto::WireFormatter::DeserializeTag(buffer, wire_type, id_number);
        }
      }

      // When an error was detect while reading the tag but no other errors where found, set it in the return value.
      if((::EmbeddedProto::Error::NO_ERRORS == return_value)
         && (::EmbeddedProto::Error::NO_ERRORS != tag_value)
         && (::EmbeddedProto::Error::END_OF_BUFFER != tag_value)) // The end of the buffer is not an array in this case.
      {
        return_value = tag_value;
      }

      return return_value;
    };

    void clear() override
    {
      clear_millis();
      clear_freq();

    }

    private:


      EmbeddedProto::uint64 millis_ = 0U;
      EmbeddedProto::floatfixed freq_ = 0.0;

};

template<uint32_t offsets_REP_LENGTH>
class doppler_array final: public ::EmbeddedProto::MessageInterface
{
  public:
    doppler_array() = default;
    doppler_array(const doppler_array& rhs )
    {
      set_offsets(rhs.get_offsets());
    }

    doppler_array(const doppler_array&& rhs ) noexcept
    {
      set_offsets(rhs.get_offsets());
    }

    ~doppler_array() override = default;

    enum class id : uint32_t
    {
      NOT_SET = 0,
      OFFSETS = 1
    };

    doppler_array& operator=(const doppler_array& rhs)
    {
      set_offsets(rhs.get_offsets());
      return *this;
    }

    doppler_array& operator=(const doppler_array&& rhs) noexcept
    {
      set_offsets(rhs.get_offsets());
      return *this;
    }

    inline const doppler_offset& offsets(uint32_t index) const { return offsets_[index]; }
    inline void clear_offsets() { offsets_.clear(); }
    inline void set_offsets(uint32_t index, const doppler_offset& value) { offsets_.set(index, value); }
    inline void set_offsets(uint32_t index, const doppler_offset&& value) { offsets_.set(index, value); }
    inline void set_offsets(const ::EmbeddedProto::RepeatedFieldFixedSize<doppler_offset, offsets_REP_LENGTH>& values) { offsets_ = values; }
    inline void add_offsets(const doppler_offset& value) { offsets_.add(value); }
    inline ::EmbeddedProto::RepeatedFieldFixedSize<doppler_offset, offsets_REP_LENGTH>& mutable_offsets() { return offsets_; }
    inline doppler_offset& mutable_offsets(uint32_t index) { return offsets_[index]; }
    inline const ::EmbeddedProto::RepeatedFieldFixedSize<doppler_offset, offsets_REP_LENGTH>& get_offsets() const { return offsets_; }
    inline const ::EmbeddedProto::RepeatedFieldFixedSize<doppler_offset, offsets_REP_LENGTH>& offsets() const { return offsets_; }


    ::EmbeddedProto::Error serialize(::EmbeddedProto::WriteBufferInterface& buffer) const override
    {
      ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;

      if(::EmbeddedProto::Error::NO_ERRORS == return_value)
      {
        return_value = offsets_.serialize_with_id(static_cast<uint32_t>(id::OFFSETS), buffer, false);
      }

      return return_value;
    };

    ::EmbeddedProto::Error deserialize(::EmbeddedProto::ReadBufferInterface& buffer) override
    {
      ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;
      ::EmbeddedProto::WireFormatter::WireType wire_type = ::EmbeddedProto::WireFormatter::WireType::VARINT;
      uint32_t id_number = 0;
      id id_tag = id::NOT_SET;

      ::EmbeddedProto::Error tag_value = ::EmbeddedProto::WireFormatter::DeserializeTag(buffer, wire_type, id_number);
      while((::EmbeddedProto::Error::NO_ERRORS == return_value) && (::EmbeddedProto::Error::NO_ERRORS == tag_value))
      {
        id_tag = static_cast<id>(id_number);
        switch(id_tag)
        {
          case id::OFFSETS:
            return_value = offsets_.deserialize_check_type(buffer, wire_type);
            break;

          case id::NOT_SET:
            return_value = ::EmbeddedProto::Error::INVALID_FIELD_ID;
            break;

          default:
            return_value = skip_unknown_field(buffer, wire_type);
            break;
        }

        if(::EmbeddedProto::Error::NO_ERRORS == return_value)
        {
          // Read the next tag.
          tag_value = ::EmbeddedProto::WireFormatter::DeserializeTag(buffer, wire_type, id_number);
        }
      }

      // When an error was detect while reading the tag but no other errors where found, set it in the return value.
      if((::EmbeddedProto::Error::NO_ERRORS == return_value)
         && (::EmbeddedProto::Error::NO_ERRORS != tag_value)
         && (::EmbeddedProto::Error::END_OF_BUFFER != tag_value)) // The end of the buffer is not an array in this case.
      {
        return_value = tag_value;
      }

      return return_value;
    };

    void clear() override
    {
      clear_offsets();

    }

    private:


      ::EmbeddedProto::RepeatedFieldFixedSize<doppler_offset, offsets_REP_LENGTH> offsets_;

};

class doppler_poly final: public ::EmbeddedProto::MessageInterface
{
  public:
    doppler_poly() = default;
    doppler_poly(const doppler_poly& rhs )
    {
      set_A(rhs.get_A());
      set_B(rhs.get_B());
      set_C(rhs.get_C());
    }

    doppler_poly(const doppler_poly&& rhs ) noexcept
    {
      set_A(rhs.get_A());
      set_B(rhs.get_B());
      set_C(rhs.get_C());
    }

    ~doppler_poly() override = default;

    enum class id : uint32_t
    {
      NOT_SET = 0,
      A = 1,
      B = 2,
      C = 3
    };

    doppler_poly& operator=(const doppler_poly& rhs)
    {
      set_A(rhs.get_A());
      set_B(rhs.get_B());
      set_C(rhs.get_C());
      return *this;
    }

    doppler_poly& operator=(const doppler_poly&& rhs) noexcept
    {
      set_A(rhs.get_A());
      set_B(rhs.get_B());
      set_C(rhs.get_C());
      return *this;
    }

    inline void clear_A() { A_.clear(); }
    inline void set_A(const EmbeddedProto::floatfixed& value) { A_ = value; }
    inline void set_A(const EmbeddedProto::floatfixed&& value) { A_ = value; }
    inline EmbeddedProto::floatfixed& mutable_A() { return A_; }
    inline const EmbeddedProto::floatfixed& get_A() const { return A_; }
    inline EmbeddedProto::floatfixed::FIELD_TYPE A() const { return A_.get(); }

    inline void clear_B() { B_.clear(); }
    inline void set_B(const EmbeddedProto::floatfixed& value) { B_ = value; }
    inline void set_B(const EmbeddedProto::floatfixed&& value) { B_ = value; }
    inline EmbeddedProto::floatfixed& mutable_B() { return B_; }
    inline const EmbeddedProto::floatfixed& get_B() const { return B_; }
    inline EmbeddedProto::floatfixed::FIELD_TYPE B() const { return B_.get(); }

    inline void clear_C() { C_.clear(); }
    inline void set_C(const EmbeddedProto::floatfixed& value) { C_ = value; }
    inline void set_C(const EmbeddedProto::floatfixed&& value) { C_ = value; }
    inline EmbeddedProto::floatfixed& mutable_C() { return C_; }
    inline const EmbeddedProto::floatfixed& get_C() const { return C_; }
    inline EmbeddedProto::floatfixed::FIELD_TYPE C() const { return C_.get(); }


    ::EmbeddedProto::Error serialize(::EmbeddedProto::WriteBufferInterface& buffer) const override
    {
      ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;

      if((0.0 != A_.get()) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        return_value = A_.serialize_with_id(static_cast<uint32_t>(id::A), buffer, false);
      }

      if((0.0 != B_.get()) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        return_value = B_.serialize_with_id(static_cast<uint32_t>(id::B), buffer, false);
      }

      if((0.0 != C_.get()) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        return_value = C_.serialize_with_id(static_cast<uint32_t>(id::C), buffer, false);
      }

      return return_value;
    };

    ::EmbeddedProto::Error deserialize(::EmbeddedProto::ReadBufferInterface& buffer) override
    {
      ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;
      ::EmbeddedProto::WireFormatter::WireType wire_type = ::EmbeddedProto::WireFormatter::WireType::VARINT;
      uint32_t id_number = 0;
      id id_tag = id::NOT_SET;

      ::EmbeddedProto::Error tag_value = ::EmbeddedProto::WireFormatter::DeserializeTag(buffer, wire_type, id_number);
      while((::EmbeddedProto::Error::NO_ERRORS == return_value) && (::EmbeddedProto::Error::NO_ERRORS == tag_value))
      {
        id_tag = static_cast<id>(id_number);
        switch(id_tag)
        {
          case id::A:
            return_value = A_.deserialize_check_type(buffer, wire_type);
            break;

          case id::B:
            return_value = B_.deserialize_check_type(buffer, wire_type);
            break;

          case id::C:
            return_value = C_.deserialize_check_type(buffer, wire_type);
            break;

          case id::NOT_SET:
            return_value = ::EmbeddedProto::Error::INVALID_FIELD_ID;
            break;

          default:
            return_value = skip_unknown_field(buffer, wire_type);
            break;
        }

        if(::EmbeddedProto::Error::NO_ERRORS == return_value)
        {
          // Read the next tag.
          tag_value = ::EmbeddedProto::WireFormatter::DeserializeTag(buffer, wire_type, id_number);
        }
      }

      // When an error was detect while reading the tag but no other errors where found, set it in the return value.
      if((::EmbeddedProto::Error::NO_ERRORS == return_value)
         && (::EmbeddedProto::Error::NO_ERRORS != tag_value)
         && (::EmbeddedProto::Error::END_OF_BUFFER != tag_value)) // The end of the buffer is not an array in this case.
      {
        return_value = tag_value;
      }

      return return_value;
    };

    void clear() override
    {
      clear_A();
      clear_B();
      clear_C();

    }

    private:


      EmbeddedProto::floatfixed A_ = 0.0;
      EmbeddedProto::floatfixed B_ = 0.0;
      EmbeddedProto::floatfixed C_ = 0.0;

};

template<uint32_t reference_LENGTH, 
uint32_t array_offsets_REP_LENGTH>
class frequency final: public ::EmbeddedProto::MessageInterface
{
  public:
    frequency() = default;
    frequency(const frequency& rhs )
    {
      set_reference(rhs.get_reference());
      if(rhs.get_which_method() != which_method_)
      {
        // First delete the old object in the oneof.
        clear_method();
      }

      switch(rhs.get_which_method())
      {
        case id::ARRAY:
          set_array(rhs.get_array());
          break;

        case id::POLY:
          set_poly(rhs.get_poly());
          break;

        default:
          break;
      }

    }

    frequency(const frequency&& rhs ) noexcept
    {
      set_reference(rhs.get_reference());
      if(rhs.get_which_method() != which_method_)
      {
        // First delete the old object in the oneof.
        clear_method();
      }

      switch(rhs.get_which_method())
      {
        case id::ARRAY:
          set_array(rhs.get_array());
          break;

        case id::POLY:
          set_poly(rhs.get_poly());
          break;

        default:
          break;
      }

    }

    ~frequency() override = default;

    enum class id : uint32_t
    {
      NOT_SET = 0,
      REFERENCE = 1,
      ARRAY = 2,
      POLY = 3
    };

    frequency& operator=(const frequency& rhs)
    {
      set_reference(rhs.get_reference());
      if(rhs.get_which_method() != which_method_)
      {
        // First delete the old object in the oneof.
        clear_method();
      }

      switch(rhs.get_which_method())
      {
        case id::ARRAY:
          set_array(rhs.get_array());
          break;

        case id::POLY:
          set_poly(rhs.get_poly());
          break;

        default:
          break;
      }

      return *this;
    }

    frequency& operator=(const frequency&& rhs) noexcept
    {
      set_reference(rhs.get_reference());
      if(rhs.get_which_method() != which_method_)
      {
        // First delete the old object in the oneof.
        clear_method();
      }

      switch(rhs.get_which_method())
      {
        case id::ARRAY:
          set_array(rhs.get_array());
          break;

        case id::POLY:
          set_poly(rhs.get_poly());
          break;

        default:
          break;
      }

      return *this;
    }

    inline void clear_reference() { reference_.clear(); }
    inline ::EmbeddedProto::FieldString<reference_LENGTH>& mutable_reference() { return reference_; }
    inline void set_reference(const ::EmbeddedProto::FieldString<reference_LENGTH>& rhs) { reference_.set(rhs); }
    inline const ::EmbeddedProto::FieldString<reference_LENGTH>& get_reference() const { return reference_; }
    inline const char* reference() const { return reference_.get_const(); }

    id get_which_method() const { return which_method_; }

    inline bool has_array() const
    {
      return id::ARRAY == which_method_;
    }
    inline void clear_array()
    {
      if(id::ARRAY == which_method_)
      {
        which_method_ = id::NOT_SET;
        method_.array_.~doppler_array<array_offsets_REP_LENGTH>();
      }
    }
    inline void set_array(const doppler_array<array_offsets_REP_LENGTH>& value)
    {
      if(id::ARRAY != which_method_)
      {
        init_method(id::ARRAY);
      }
      method_.array_ = value;
    }
    inline void set_array(const doppler_array<array_offsets_REP_LENGTH>&& value)
    {
      if(id::ARRAY != which_method_)
      {
        init_method(id::ARRAY);
      }
      method_.array_ = value;
    }
    inline doppler_array<array_offsets_REP_LENGTH>& mutable_array()
    {
      if(id::ARRAY != which_method_)
      {
        init_method(id::ARRAY);
      }
      return method_.array_;
    }
    inline const doppler_array<array_offsets_REP_LENGTH>& get_array() const { return method_.array_; }
    inline const doppler_array<array_offsets_REP_LENGTH>& array() const { return method_.array_; }

    inline bool has_poly() const
    {
      return id::POLY == which_method_;
    }
    inline void clear_poly()
    {
      if(id::POLY == which_method_)
      {
        which_method_ = id::NOT_SET;
        method_.poly_.~doppler_poly();
      }
    }
    inline void set_poly(const doppler_poly& value)
    {
      if(id::POLY != which_method_)
      {
        init_method(id::POLY);
      }
      method_.poly_ = value;
    }
    inline void set_poly(const doppler_poly&& value)
    {
      if(id::POLY != which_method_)
      {
        init_method(id::POLY);
      }
      method_.poly_ = value;
    }
    inline doppler_poly& mutable_poly()
    {
      if(id::POLY != which_method_)
      {
        init_method(id::POLY);
      }
      return method_.poly_;
    }
    inline const doppler_poly& get_poly() const { return method_.poly_; }
    inline const doppler_poly& poly() const { return method_.poly_; }


    ::EmbeddedProto::Error serialize(::EmbeddedProto::WriteBufferInterface& buffer) const override
    {
      ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;

      if(::EmbeddedProto::Error::NO_ERRORS == return_value)
      {
        return_value = reference_.serialize_with_id(static_cast<uint32_t>(id::REFERENCE), buffer, false);
      }

      switch(which_method_)
      {
        case id::ARRAY:
          if(has_array() && (::EmbeddedProto::Error::NO_ERRORS == return_value))
          {
            return_value = method_.array_.serialize_with_id(static_cast<uint32_t>(id::ARRAY), buffer, true);
          }
          break;

        case id::POLY:
          if(has_poly() && (::EmbeddedProto::Error::NO_ERRORS == return_value))
          {
            return_value = method_.poly_.serialize_with_id(static_cast<uint32_t>(id::POLY), buffer, true);
          }
          break;

        default:
          break;
      }

      return return_value;
    };

    ::EmbeddedProto::Error deserialize(::EmbeddedProto::ReadBufferInterface& buffer) override
    {
      ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;
      ::EmbeddedProto::WireFormatter::WireType wire_type = ::EmbeddedProto::WireFormatter::WireType::VARINT;
      uint32_t id_number = 0;
      id id_tag = id::NOT_SET;

      ::EmbeddedProto::Error tag_value = ::EmbeddedProto::WireFormatter::DeserializeTag(buffer, wire_type, id_number);
      while((::EmbeddedProto::Error::NO_ERRORS == return_value) && (::EmbeddedProto::Error::NO_ERRORS == tag_value))
      {
        id_tag = static_cast<id>(id_number);
        switch(id_tag)
        {
          case id::REFERENCE:
            return_value = reference_.deserialize_check_type(buffer, wire_type);
            break;

          case id::ARRAY:
            return_value = deserialize_method(id::ARRAY, method_.array_, buffer, wire_type);

            break;

          case id::POLY:
            return_value = deserialize_method(id::POLY, method_.poly_, buffer, wire_type);

            break;

          case id::NOT_SET:
            return_value = ::EmbeddedProto::Error::INVALID_FIELD_ID;
            break;

          default:
            return_value = skip_unknown_field(buffer, wire_type);
            break;
        }

        if(::EmbeddedProto::Error::NO_ERRORS == return_value)
        {
          // Read the next tag.
          tag_value = ::EmbeddedProto::WireFormatter::DeserializeTag(buffer, wire_type, id_number);
        }
      }

      // When an error was detect while reading the tag but no other errors where found, set it in the return value.
      if((::EmbeddedProto::Error::NO_ERRORS == return_value)
         && (::EmbeddedProto::Error::NO_ERRORS != tag_value)
         && (::EmbeddedProto::Error::END_OF_BUFFER != tag_value)) // The end of the buffer is not an array in this case.
      {
        return_value = tag_value;
      }

      return return_value;
    };

    void clear() override
    {
      clear_reference();
      clear_method();

    }

    private:


      ::EmbeddedProto::FieldString<reference_LENGTH> reference_;

      id which_method_ = id::NOT_SET;
      union method
      {
        method() {}
        ~method() {}
        doppler_array<array_offsets_REP_LENGTH> array_;
        doppler_poly poly_;
      };
      method method_;

      void init_method(const id field_id)
      {
        if(id::NOT_SET != which_method_)
        {
          // First delete the old object in the oneof.
          clear_method();
        }

        // C++11 unions only support nontrivial members when you explicitly call the placement new statement.
        switch(field_id)
        {
          case id::ARRAY:
            new(&method_.array_) doppler_array<array_offsets_REP_LENGTH>;
            which_method_ = id::ARRAY;
            break;

          case id::POLY:
            new(&method_.poly_) doppler_poly;
            which_method_ = id::POLY;
            break;

          default:
            break;
         }

         which_method_ = field_id;
      }

      void clear_method()
      {
        switch(which_method_)
        {
          case id::ARRAY:
            method_.array_.~doppler_array<array_offsets_REP_LENGTH>(); // NOSONAR Unions require this.
            break;
          case id::POLY:
            method_.poly_.~doppler_poly(); // NOSONAR Unions require this.
            break;
          default:
            break;
        }
        which_method_ = id::NOT_SET;
      }

      ::EmbeddedProto::Error deserialize_method(const id field_id, ::EmbeddedProto::Field& field,
                                    ::EmbeddedProto::ReadBufferInterface& buffer,
                                    const ::EmbeddedProto::WireFormatter::WireType wire_type)
      {
        if(field_id != which_method_)
        {
          init_method(field_id);
        }
        ::EmbeddedProto::Error return_value = field.deserialize_check_type(buffer, wire_type);
        if(::EmbeddedProto::Error::NO_ERRORS != return_value)
        {
          clear_method();
        }
        return return_value;
      }

};

#endif // FREQUENCY_H