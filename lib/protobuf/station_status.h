/*
 *  Copyright (C) 2020-2021 Embedded AMS B.V. - All Rights Reserved
 *
 *  This file is part of Embedded Proto.
 *
 *  Embedded Proto is open source software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 *  Embedded Proto  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Embedded Proto. If not, see <https://www.gnu.org/licenses/>.
 *
 *  For commercial and closed source application please visit:
 *  <https://EmbeddedProto.com/license/>.
 *
 *  Embedded AMS B.V.
 *  Info:
 *    info at EmbeddedProto dot com
 *
 *  Postal address:
 *    Johan Huizingalaan 763a
 *    1066 VH, Amsterdam
 *    the Netherlands
 */

// This file is generated. Please do not edit!
#ifndef STATION_STATUS_H
#define STATION_STATUS_H

#include <cstdint>
#include <MessageInterface.h>
#include <WireFormatter.h>
#include <Fields.h>
#include <MessageSizeCalculator.h>
#include <ReadBufferSection.h>
#include <RepeatedFieldFixedSize.h>
#include <FieldStringBytes.h>
#include <Errors.h>
#include <limits>

// Include external proto definitions


template<uint32_t current_datetime_LENGTH>
class ground_station_status final: public ::EmbeddedProto::MessageInterface
{
  public:
    ground_station_status() = default;
    ground_station_status(const ground_station_status& rhs )
    {
      set_current_datetime(rhs.get_current_datetime());
      set_in_rx(rhs.get_in_rx());
      set_mode(rhs.get_mode());
      set_rx_freq(rhs.get_rx_freq());
      set_frames_cnt(rhs.get_frames_cnt());
    }

    ground_station_status(const ground_station_status&& rhs ) noexcept
    {
      set_current_datetime(rhs.get_current_datetime());
      set_in_rx(rhs.get_in_rx());
      set_mode(rhs.get_mode());
      set_rx_freq(rhs.get_rx_freq());
      set_frames_cnt(rhs.get_frames_cnt());
    }

    ~ground_station_status() override = default;

    enum class modulation_mode : uint32_t
    {
      FSK = 0,
      LORA = 1
    };

    enum class id : uint32_t
    {
      NOT_SET = 0,
      CURRENT_DATETIME = 1,
      IN_RX = 2,
      MODE = 3,
      RX_FREQ = 4,
      FRAMES_CNT = 5
    };

    ground_station_status& operator=(const ground_station_status& rhs)
    {
      set_current_datetime(rhs.get_current_datetime());
      set_in_rx(rhs.get_in_rx());
      set_mode(rhs.get_mode());
      set_rx_freq(rhs.get_rx_freq());
      set_frames_cnt(rhs.get_frames_cnt());
      return *this;
    }

    ground_station_status& operator=(const ground_station_status&& rhs) noexcept
    {
      set_current_datetime(rhs.get_current_datetime());
      set_in_rx(rhs.get_in_rx());
      set_mode(rhs.get_mode());
      set_rx_freq(rhs.get_rx_freq());
      set_frames_cnt(rhs.get_frames_cnt());
      return *this;
    }

    inline void clear_current_datetime() { current_datetime_.clear(); }
    inline ::EmbeddedProto::FieldString<current_datetime_LENGTH>& mutable_current_datetime() { return current_datetime_; }
    inline void set_current_datetime(const ::EmbeddedProto::FieldString<current_datetime_LENGTH>& rhs) { current_datetime_.set(rhs); }
    inline const ::EmbeddedProto::FieldString<current_datetime_LENGTH>& get_current_datetime() const { return current_datetime_; }
    inline const char* current_datetime() const { return current_datetime_.get_const(); }

    inline void clear_in_rx() { in_rx_.clear(); }
    inline void set_in_rx(const EmbeddedProto::boolean& value) { in_rx_ = value; }
    inline void set_in_rx(const EmbeddedProto::boolean&& value) { in_rx_ = value; }
    inline EmbeddedProto::boolean& mutable_in_rx() { return in_rx_; }
    inline const EmbeddedProto::boolean& get_in_rx() const { return in_rx_; }
    inline EmbeddedProto::boolean::FIELD_TYPE in_rx() const { return in_rx_.get(); }

    inline void clear_mode() { mode_ = static_cast<modulation_mode>(0); }
    inline void set_mode(const modulation_mode& value) { mode_ = value; }
    inline void set_mode(const modulation_mode&& value) { mode_ = value; }
    inline const modulation_mode& get_mode() const { return mode_; }
    inline modulation_mode mode() const { return mode_; }

    inline void clear_rx_freq() { rx_freq_.clear(); }
    inline void set_rx_freq(const EmbeddedProto::floatfixed& value) { rx_freq_ = value; }
    inline void set_rx_freq(const EmbeddedProto::floatfixed&& value) { rx_freq_ = value; }
    inline EmbeddedProto::floatfixed& mutable_rx_freq() { return rx_freq_; }
    inline const EmbeddedProto::floatfixed& get_rx_freq() const { return rx_freq_; }
    inline EmbeddedProto::floatfixed::FIELD_TYPE rx_freq() const { return rx_freq_.get(); }

    inline void clear_frames_cnt() { frames_cnt_.clear(); }
    inline void set_frames_cnt(const EmbeddedProto::uint64& value) { frames_cnt_ = value; }
    inline void set_frames_cnt(const EmbeddedProto::uint64&& value) { frames_cnt_ = value; }
    inline EmbeddedProto::uint64& mutable_frames_cnt() { return frames_cnt_; }
    inline const EmbeddedProto::uint64& get_frames_cnt() const { return frames_cnt_; }
    inline EmbeddedProto::uint64::FIELD_TYPE frames_cnt() const { return frames_cnt_.get(); }


    ::EmbeddedProto::Error serialize(::EmbeddedProto::WriteBufferInterface& buffer) const override
    {
      ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;

      if(::EmbeddedProto::Error::NO_ERRORS == return_value)
      {
        return_value = current_datetime_.serialize_with_id(static_cast<uint32_t>(id::CURRENT_DATETIME), buffer, false);
      }

      if((false != in_rx_.get()) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        return_value = in_rx_.serialize_with_id(static_cast<uint32_t>(id::IN_RX), buffer, false);
      }

      if((static_cast<modulation_mode>(0) != mode_) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        EmbeddedProto::uint32 value = 0;
        value.set(static_cast<uint32_t>(mode_));
        return_value = value.serialize_with_id(static_cast<uint32_t>(id::MODE), buffer, false);
      }

      if((0.0 != rx_freq_.get()) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        return_value = rx_freq_.serialize_with_id(static_cast<uint32_t>(id::RX_FREQ), buffer, false);
      }

      if((0U != frames_cnt_.get()) && (::EmbeddedProto::Error::NO_ERRORS == return_value))
      {
        return_value = frames_cnt_.serialize_with_id(static_cast<uint32_t>(id::FRAMES_CNT), buffer, false);
      }

      return return_value;
    };

    ::EmbeddedProto::Error deserialize(::EmbeddedProto::ReadBufferInterface& buffer) override
    {
      ::EmbeddedProto::Error return_value = ::EmbeddedProto::Error::NO_ERRORS;
      ::EmbeddedProto::WireFormatter::WireType wire_type = ::EmbeddedProto::WireFormatter::WireType::VARINT;
      uint32_t id_number = 0;
      id id_tag = id::NOT_SET;

      ::EmbeddedProto::Error tag_value = ::EmbeddedProto::WireFormatter::DeserializeTag(buffer, wire_type, id_number);
      while((::EmbeddedProto::Error::NO_ERRORS == return_value) && (::EmbeddedProto::Error::NO_ERRORS == tag_value))
      {
        id_tag = static_cast<id>(id_number);
        switch(id_tag)
        {
          case id::CURRENT_DATETIME:
            return_value = current_datetime_.deserialize_check_type(buffer, wire_type);
            break;

          case id::IN_RX:
            return_value = in_rx_.deserialize_check_type(buffer, wire_type);
            break;

          case id::MODE:
            if(::EmbeddedProto::WireFormatter::WireType::VARINT == wire_type)
            {
              uint32_t value = 0;
              return_value = ::EmbeddedProto::WireFormatter::DeserializeVarint(buffer, value);
              if(::EmbeddedProto::Error::NO_ERRORS == return_value)
              {
                set_mode(static_cast<modulation_mode>(value));
              }
            }
            else
            {
              // Wire type does not match field.
              return_value = ::EmbeddedProto::Error::INVALID_WIRETYPE;
            }
            break;

          case id::RX_FREQ:
            return_value = rx_freq_.deserialize_check_type(buffer, wire_type);
            break;

          case id::FRAMES_CNT:
            return_value = frames_cnt_.deserialize_check_type(buffer, wire_type);
            break;

          case id::NOT_SET:
            return_value = ::EmbeddedProto::Error::INVALID_FIELD_ID;
            break;

          default:
            return_value = skip_unknown_field(buffer, wire_type);
            break;
        }

        if(::EmbeddedProto::Error::NO_ERRORS == return_value)
        {
          // Read the next tag.
          tag_value = ::EmbeddedProto::WireFormatter::DeserializeTag(buffer, wire_type, id_number);
        }
      }

      // When an error was detect while reading the tag but no other errors where found, set it in the return value.
      if((::EmbeddedProto::Error::NO_ERRORS == return_value)
         && (::EmbeddedProto::Error::NO_ERRORS != tag_value)
         && (::EmbeddedProto::Error::END_OF_BUFFER != tag_value)) // The end of the buffer is not an array in this case.
      {
        return_value = tag_value;
      }

      return return_value;
    };

    void clear() override
    {
      clear_current_datetime();
      clear_in_rx();
      clear_mode();
      clear_rx_freq();
      clear_frames_cnt();

    }

    private:


      ::EmbeddedProto::FieldString<current_datetime_LENGTH> current_datetime_;
      EmbeddedProto::boolean in_rx_ = false;
      modulation_mode mode_ = static_cast<modulation_mode>(0);
      EmbeddedProto::floatfixed rx_freq_ = 0.0;
      EmbeddedProto::uint64 frames_cnt_ = 0U;

};

#endif // STATION_STATUS_H